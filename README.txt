-- SUMMARY --

The CommWeb Virtual Payment Client Integration (3-Party) module which helps to use the Commonwealth Bank CommWeb service for payment.

For a full description of the module, visit the project page:
  http://drupal.org/project/cwpayment

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/cwpayment